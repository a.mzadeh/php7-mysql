<?php

/**
 * Ali MoghaddasZadeh
 * a.mzadeh@gmail.com
 */

function mysql_connect($host, $user, $password) {
    return array (
        'host' => $host,
        'username' => $user,
        'password' => $password
    );
}

function mysql_select_db($db, &$link) {
    $link = new PDO('mysql:host='.$link['host'].';dbname='.$db, $link['username'], $link['password']);
}

function mysql_query($query, $link) {
    $sth = $link->prepare($query);
    $sth->execute();
    return $sth;
}

function mysql_fetch_array($sth) {
    return $sth->fetch();
}

//function mysql_error($link = null) {
//    if (!$link) return null;
//    return $link->errorInfo();
//}

function mysql_escape_string($value) {
    $search = array("\\",  "\x00", "\n",  "\r",  "'",  '"', "\x1a");
    $replace = array("\\\\","\\0","\\n", "\\r", "\'", '\"', "\\Z");

    return str_replace($search, $replace, $value);
}

function mysql_real_escape_string($value) {
    return mysql_escape_string($value);
}